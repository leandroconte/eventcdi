package br.com.lnd.testeobserver.observer;

import br.com.lnd.testeobserver.dto.CarObserverDTO;
import br.com.lnd.testeobserver.service.CarService;

import javax.enterprise.event.Observes;
import javax.enterprise.event.TransactionPhase;
import javax.inject.Inject;
import javax.transaction.Transactional;

public class ObserverCar {

    @Inject
    private CarService carService;

    //    @Transactional(Transactional.TxType.NOT_SUPPORTED)
    public void observerCar(@Observes(during = TransactionPhase.AFTER_SUCCESS) CarObserverDTO carObserverDTO) {
        saveCar(carObserverDTO);

    }

    @Transactional(Transactional.TxType.REQUIRES_NEW)
    private void saveCar(CarObserverDTO carObserverDTO) {
        carService.save(carObserverDTO.getCar());
//        throw new CarException("awdawd");
    }

}
