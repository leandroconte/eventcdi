package br.com.lnd.testeobserver.model;

import br.com.lnd.testeobserver.dto.CarDTO;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.UUID;

@Entity
@Table(name = "cars")
public class Car implements Serializable {

    public Car() {
    }

    public Car(CarDTO carDTO) {
        this.id = UUID.randomUUID().toString();
        this.brand = carDTO.getBrand();
        this.type = carDTO.getType();
    }

    @Id
    @Column(name = "id")
    private String id;

    @Column(name = "brand")
    private String brand;

    @Column(name = "type")
    private String type;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
