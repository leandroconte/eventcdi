package br.com.lnd.testeobserver.dto;

import java.io.Serializable;

public class CarDTO implements Serializable {

    private String id;
    private String brand;
    private String type;

    public CarDTO(String brand, String type) {
        this.brand = brand;
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
