package br.com.lnd.testeobserver.dto;

public class CarObserverDTO {

    private CarDTO carDTO;

    public CarObserverDTO(CarDTO carDTO) {
        this.carDTO = carDTO;
    }

    public CarDTO getCar() {
        return carDTO;
    }
}
