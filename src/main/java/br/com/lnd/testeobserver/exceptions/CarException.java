package br.com.lnd.testeobserver.exceptions;

public class CarException extends Exception {
    public CarException(String message) {
        super(message);
    }
}
