package br.com.lnd.testeobserver.rest;

import br.com.lnd.testeobserver.dao.CarDAO;
import br.com.lnd.testeobserver.dto.CarDTO;
import br.com.lnd.testeobserver.model.Car;
import br.com.lnd.testeobserver.service.FireCar;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;
import java.util.UUID;

@Path("/cars")
public class CarRest {

    @Inject
    private FireCar fireCar;

    @Inject
    private CarDAO carDAO;

    @GET
    public String save() {
        CarDTO carDTO = new CarDTO( "Test", "Type 1");
        fireCar.fireCar(carDTO);
        return "Firing car";
    }

    @GET
    @Path("all")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Car> listAll() {
        return carDAO.listAll();
    }

}
