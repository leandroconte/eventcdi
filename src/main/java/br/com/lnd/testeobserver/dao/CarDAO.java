package br.com.lnd.testeobserver.dao;

import br.com.lnd.testeobserver.model.Car;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

public class CarDAO {

    @PersistenceContext(name = "test")
    private EntityManager entityManager;

    public void save(Car car) {
        entityManager.persist(car);
    }

    public List<Car> listAll() {
        return entityManager.createQuery("select c from " + Car.class.getSimpleName() + " c ", Car.class)
                .getResultList();
    }

}
