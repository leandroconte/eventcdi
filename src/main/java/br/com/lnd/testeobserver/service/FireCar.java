package br.com.lnd.testeobserver.service;

import br.com.lnd.testeobserver.dao.CarDAO;
import br.com.lnd.testeobserver.dto.CarDTO;
import br.com.lnd.testeobserver.dto.CarObserverDTO;
import br.com.lnd.testeobserver.model.Car;

import javax.ejb.Stateless;
import javax.enterprise.event.Event;
import javax.inject.Inject;

@Stateless
public class FireCar {

    @Inject
    private Event<CarObserverDTO> carObserverDTOEvent;

    @Inject
    private CarDAO carDAO;

    //    @Asynchronous
    public void fireCar(CarDTO carDTO) {
        carObserverDTOEvent.fire(new CarObserverDTO(carDTO));

        carDTO.setBrand("Batata");
        carDAO.save(new Car(carDTO));
    }

}
