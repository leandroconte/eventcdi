package br.com.lnd.testeobserver.service;

import br.com.lnd.testeobserver.dao.CarDAO;
import br.com.lnd.testeobserver.dto.CarDTO;
import br.com.lnd.testeobserver.model.Car;

import javax.inject.Inject;

public class CarService {

    @Inject
    private CarDAO carDAO;

    public void save(CarDTO carDTO) {
        carDAO.save(new Car(carDTO));
    }

}
