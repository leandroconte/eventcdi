package br.com.lnd.testeobserver;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath("/test")
public class TesteObserverApplication extends Application {

}
